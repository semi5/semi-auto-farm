export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v11987`, ...msg);
		};

		// script details
		const id = 'auto-farm';
		const title = 'SEMI Auto Farming';

		// has Into the Abyss DLC
		const hasIntoTheAbyss = cloudManager.hasItAEntitlementAndIsEnabled;

		// plot states
		const STATE_LOCKED = 0;
		const STATE_EMPTY = 1;
		const STATE_GROWING = 2;
		const STATE_GROWN = 3;
		const STATE_DEAD = 4;

		// setting groups
		const SETTING_COMPOST = ctx.settings.section('Compost');
		const SETTING_POTION = ctx.settings.section('Auto Swap Potions');
		const SETTING_EQUIPMENT = ctx.settings.section('Auto Swap Equipment');

		// planting priorities
		const priorityToConfig = (arr) => arr.map(action => action.id);
		const priorityFromConfig = (arr) => arr.map(actionID => getAction(actionID));
		const priorityTypes = {
			custom: {
				id: 'custom',
				description: 'Custom priority',
				tooltip: 'Drag seeds to change their priority'
			},
			mastery: {
				id: 'mastery',
				description: 'Highest mastery',
				tooltip: 'Seeds with maxed mastery are excluded<br>Click seeds to disable/enable them',
			},
			replant: {
				id: 'replant',
				description: 'Replant',
				tooltip: 'Lock patches to their current seeds'
			},
			lowestQuantity: {
				id: 'lowestQuantity',
				description: 'Lowest Quantity',
				tooltip: 'Crops with the lowest quantity in the bank are planted',
			}
		};

		let config = {
			disabledActions: {}
		};

		const PRIORITY_ARRAYS = {};
		const FARMING_SKILL = game.farming;
		const FARMING_CATEGORIES = FARMING_SKILL.categories.allObjects;
		const FARMING_ACTIONS = {};
		const FARMING_PLOTS = {};
		FARMING_CATEGORIES.forEach(category => {
			FARMING_ACTIONS[category.id] = FARMING_SKILL.actions.filter(action => action.category == category).sort((a, b) => b.level - a.level);
			FARMING_PLOTS[category.id] = FARMING_SKILL.plots.filter(plot => plot.category == category).sort((a, b) => a.level - b.level);
			PRIORITY_ARRAYS[category.id] = [...FARMING_ACTIONS[category.id]];

			config[category.id] = {
				enabled: false,
				priorityType: priorityTypes.custom.id,
				priority: priorityToConfig(PRIORITY_ARRAYS[category.id]),
				lockedPlots: {},
				reservedPlots: {},
			};
		});

		// items
		const shopCompost = game.shop.purchases.getObjectByID('melvorD:Compost');
		const itemCompost = game.items.getObjectByID('melvorD:Compost');
		const itemWeirdGloop = game.items.getObjectByID('melvorD:Weird_Gloop');
		const itemAbyssalCompost = game.items.getObjectByID('melvorItA:Abyssal_Compost');

		// potions
		const gloomgrowthPotions = [
			game.items.getObjectByID('melvorItA:Gloomgrowth_Potion_IV'),
			game.items.getObjectByID('melvorItA:Gloomgrowth_Potion_III'),
			game.items.getObjectByID('melvorItA:Gloomgrowth_Potion_II'),
			game.items.getObjectByID('melvorItA:Gloomgrowth_Potion_I')
		];
		  
		const generousHarvestPotions = [
			game.items.getObjectByID('melvorF:Farming_Potion_IV'),
			game.items.getObjectByID('melvorF:Farming_Potion_III'),
			game.items.getObjectByID('melvorF:Farming_Potion_II'),
			game.items.getObjectByID('melvorF:Farming_Potion_I')
		];

		// utils
		const getAction = (id) => FARMING_SKILL.actions.getObjectByID(id);
		const getMasteryLevel = (action) => FARMING_SKILL.getMasteryLevel(action);
		const getMasteryXP = (action) => FARMING_SKILL.getMasteryXP(action);

		const bankQty = (item) => game.bank.getQty(item);
		const bankHasRoom = (item) => game.bank.occupiedSlots < game.bank.maximumSlots || item == null || bankQty(item) > 0;

		const getProduct = (action) => action.product;
		const canAfford = (action) => bankQty(action.seedCost.item) >= action.seedCost.quantity;
		const canCropDie = (action, plot) => !(getMasteryLevel(action) >= 50 || game.modifiers.farmingCropsCannotDie > 0);

		const actionIsValid = (skill, action) => {
			return skill.level >= action.level && skill.abyssalLevel >= action.abyssalLevel && action.realm.isUnlocked;
		}

		// equipment
		const getGearSlot = (slotID) => game.combat.player.equipment.equippedItems[slotID];
		const canEquipGearItem = (item) => game.checkRequirements(item.equipRequirements, false);
		const isItemEquipped = (item) => game.combat.player.equipment.getSlotOfItem(item) != undefined;
		const equipGearItem = (slotID, item, qty = 1000000) => {
			const slot = game.equipmentSlots.getObjectByID(slotID);
			if (!slot) return false;
			game.combat.player.equipItem(item, game.combat.player.selectedEquipmentSet, slot, qty);
		};
		const gearSlots = game.equipmentSlots.allObjects.map(slot => slot.id);

		const melvorGearPriority = {};
		const gearPriorityIDs = {
			'melvorD:Weapon': [
				'melvorItA:Bobs_Lost_Rake',
				'melvorAoD:Ancient_Scythe',
				'melvorD:Bobs_Rake'
			],
			'melvorD:Ring': [
				'melvorItA:Signet_Ring_of_Corrupted_Souls',
				'melvorD:Aorpheats_Signet_Ring',
				'melvorD:Ancient_Ring_Of_Skills'
			],
			'melvorD:Gloves': [
				'melvorItA:Bobs_Lost_Gloves',
				'melvorF:Bobs_Gloves'
			],
			'melvorD:Head': [
				'melvorF:Clown_Hat',
			],
			'melvorD:Cape': [
				'melvorTotH:Superior_Max_Skillcape',
				'melvorTotH:Superior_Farming_Skillcape',
				'melvorF:Cape_of_Completion',
				'melvorF:Max_Skillcape',
				'melvorD:Farming_Skillcape'
			],
			'melvorD:Consumable': [
				'melvorF:Seed_Pouch_II',
				'melvorF:Seed_Pouch'
			],
			'melvorD:Enhancement1': [
				'melvorF:Undead_Enhancement_Superior',
				'melvorF:Undead_Enhancement_3_Augmented',
				'melvorF:Undead_Enhancement_3',
			],
			'melvorD:Enhancement2': [
				'melvorF:Dragon_Enhancement_Superior',
				'melvorF:Dragon_Enhancement_2_Augmented',
				'melvorF:Dragon_Enhancement_2'
			],
		};

		const abyssalGearPriority = {};
		const abyssalGearPriorityIDs = {
			'melvorD:Weapon': [
				'melvorItA:Bobs_Lost_Rake',
				'melvorItA:Abyssal_Scythe'
			],
			'melvorD:Ring': [
				'melvorItA:Signet_Ring_of_Corrupted_Souls',
				'melvorD:Aorpheats_Signet_Ring',
				'melvorItA:Lost_Ring_of_the_Abyss'
			],
			'melvorD:Gloves': [
				'melvorItA:Bobs_Lost_Gloves',
				'melvorF:Bobs_Gloves',
				'melvorItA:Golem_Gloves'
			],
			'melvorD:Amulet': ['melvorItA:Voidtaker_Amulet'],
			'melvorD:Head': [
				'melvorF:Clown_Hat',
			],
			'melvorD:Cape': [
				'melvorTotH:Superior_Max_Skillcape',
				'melvorTotH:Superior_Farming_Skillcape',
				'melvorF:Cape_of_Completion',
				'melvorF:Max_Skillcape',
				'melvorD:Farming_Skillcape'
			],
			'melvorD:Consumable': [
				'melvorF:Seed_Pouch_II',
				'melvorF:Seed_Pouch'
			],
			'melvorD:Gem': ['melvorItA:Abyssal_Skilling_Gem'],
			'melvorD:Enhancement1': ['melvorF:Undead_Enhancement_Superior'],
			'melvorD:Enhancement2': [
				'melvorF:Dragon_Enhancement_Superior',
				'melvorF:Dragon_Enhancement_2_Augmented',
				'melvorF:Dragon_Enhancement_2'
			],
			'melvorD:Enhancement3': ['melvorItA:Overlords_Enhancement_Superior'],
		};

		gearSlots.forEach(slotID => { 
			if (gearPriorityIDs[slotID]) {
				melvorGearPriority[slotID] = [];
				gearPriorityIDs[slotID].forEach(itemID => {
					let item = game.items.getObjectByID(itemID);

					if (item)
						melvorGearPriority[slotID].push(item);
				});
			}
		});

		gearSlots.forEach(slotID => { 
			if (abyssalGearPriorityIDs[slotID]) {
				abyssalGearPriority[slotID] = [];
				abyssalGearPriorityIDs[slotID].forEach(itemID => {
					let item = game.items.getObjectByID(itemID);

					if (item)
						abyssalGearPriority[slotID].push(item);
				});
			}
		});

		const canChangeEquipment = () => {
			debugLog(`canChangeEquipment:`, game.combat.player.manager.areaType == CombatAreaType.None, !game.isGolbinRaid);
			return game.combat.player.manager.areaType == CombatAreaType.None && !game.isGolbinRaid;
		}

		const equipmentSwap = (realm) => {
			if (!canChangeEquipment())
				return
			
			const gearPriority = realm === "abyssal" ? abyssalGearPriority : melvorGearPriority;
		
			gearSlots.forEach(slotID => {
				let gear = gearPriority[slotID];
				if (gear && SETTING_EQUIPMENT.get(`swap-${slotID}`)) {
					for (let i = 0; i < gear.length; i++) {
						let toEquipItem = gear[i];
						let gearSlot = getGearSlot(slotID);
		
						if (isItemEquipped(toEquipItem)) {
							break;
						}
		
						if (bankQty(toEquipItem) <= 0 || !canEquipGearItem(toEquipItem)) {
							continue;
						}
		
						if (gearSlot.item != toEquipItem) {
							equipGearItem(slotID, toEquipItem);
							break;
						}
					}
				}
			});
		};		

		// compost
		const canBuyCompost = (total = 5) => {
			if (game.modifiers.freeCompost > 0)
				return false;

			if (!SETTING_COMPOST.get('compost-buy'))
				return false;

			return game.shop.getPurchaseCosts(shopCompost, total).checkIfOwned() && bankHasRoom(itemCompost);
		};

		const buyCompost = () => {
			let qty = bankQty(itemCompost);
			let purchaseAmount = 5 - qty;
			if (purchaseAmount > 0 && canBuyCompost(purchaseAmount)) {
				let lastBuyQty = game.shop.buyQuantity;
				game.shop.buyQuantity = purchaseAmount;
				game.shop.buyItemOnClick(shopCompost, true);
				game.shop.buyQuantity = lastBuyQty;
				debugLog(`Bought ${purchaseAmount} Compost.`);
			}
		};

		// Potion swapping stuff		
		let equippedPotion = null;

		const swapPotions = (potions) => {
			for (const potion of potions) {
				if (game.bank.hasItem(potion)) {
					usePotion(potion);
					return potion;
				}
			}
		};	  

		// Have to manually f*cking implement these two functions because otherwise the potion dialog menu will open
		const usePotion = (item) => {
			if (!game.bank.hasItem(item)) 
				return;
		
			const existingPotion = game.potions.activePotions.get(item.action);
			const charges = game.potions.getPotionCharges(item);
		
			if (existingPotion !== undefined && existingPotion.item === item) {
				existingPotion.charges = charges;
			} else {
				if (existingPotion !== undefined)
					game.events.unassignMatchers(existingPotion.unassigners);
				
				game.potions.activePotions.set(item.action, game.potions.createActivePotion(item, charges));
				game.potions.computeProvidedStats();
			}
		
			game.bank.removeItemQuantity(item, 1, true);
			game.stats.Herblore.inc(HerbloreStats.PotionsUsed);
		}

		const removePotion = (potion) => {
			if (potion) {
				const action = potion.action;
				const existingPotion = game.potions.activePotions.get(action);
				if (!existingPotion) 
					return;

				game.events.unassignMatchers(existingPotion.unassigners);
				game.potions.activePotions.delete(action);
				game.potions.computeProvidedStats();
			}
		}

		const removeGloomgrowthPotion = () => {
			if (!equippedPotion?.id) 
				return;

			for (const potion of gloomgrowthPotions) {
				if (potion.id === equippedPotion.id)
					removePotion(equippedPotion);
			}
		}

		const hasGenerousHarvestPotionEquipped = () => {
			if (!equippedPotion?.id) return false;

			for (const potion of generousHarvestPotions) {
				if (potion.id === equippedPotion.id)
					return true;
			}
		}

		// plots
		const needActionFilter = (plot) => (config[plot.category.id].enabled && !config[plot.category.id].reservedPlots[plot.id]) && (
			plot.state == STATE_EMPTY ||
			plot.state == STATE_DEAD ||
			plot.state == STATE_GROWN
		);

		const handlePlots = () => {
			const needAction = FARMING_SKILL.plots.some(plot => needActionFilter(plot));

			if (needAction) {
				let lastRealm = null;
				const initialEquipment = mod.api.SEMI.equipmentSnapshot();

				const SORTED_PLOTS = FARMING_SKILL.plots.allObjects.sort((a, b) => {
					// Define whether each plot is relevant (grown or dead)
					const aIsRelevant = a.state === STATE_GROWN || a.state === STATE_DEAD;
					const bIsRelevant = b.state === STATE_GROWN || b.state === STATE_DEAD;
				
					// If only one plot is relevant, prioritize it
					if (aIsRelevant && !bIsRelevant) return -1;
					if (!aIsRelevant && bIsRelevant) return 1;
				
					// If both plots are relevant, sort by namespace
					if (aIsRelevant && bIsRelevant) {
						if (a.plantedRecipe.namespace === "melvorItA" && b.plantedRecipe.namespace === "melvor") return 1;
						if (a.plantedRecipe.namespace === "melvor" && b.plantedRecipe.namespace === "melvorItA") return -1;
						return 0;
					}
				
					// If neither plot is relevant, they remain in the same relative order
					return 0;
				});
				
				SORTED_PLOTS.forEach(plot => {
					if (plot.state === STATE_LOCKED || plot.state === STATE_GROWING) {
						return;
					}
		
					if (!config[plot.category.id].enabled) {
						return;
					}
		
					if (config[plot.category.id].reservedPlots[plot.id] !== undefined) {
						return;
					}
		
					if (plot.state === STATE_GROWN || plot.state === STATE_DEAD) {
						const currentRealm = plot.plantedRecipe.namespace === "melvorItA" ? "abyssal" : "melvor";
					
						// Only perform an equipment swap if the realm has changed
						if (currentRealm !== lastRealm) {
							equipmentSwap(currentRealm);

							if (SETTING_POTION.get('auto-swap-potions'))
								equippedPotion = currentRealm === "abyssal" ? swapPotions([...gloomgrowthPotions, ...generousHarvestPotions]) : (hasGenerousHarvestPotionEquipped() ? null : swapPotions(generousHarvestPotions));

							lastRealm = currentRealm;
						}
					
						harvestPlot(plot);
					}					
		
					if (plot.state == STATE_EMPTY)
						seedPlot(plot);
				});
		
				FARMING_CATEGORIES.forEach(category => {
					if (config[category.id].priorityType === priorityTypes.mastery.id)
						orderMasteryPriorityMenu(category);
				});

				mod.api.SEMI.equipmentSnapshotRestore(initialEquipment);

				if (hasIntoTheAbyss && SETTING_POTION.get('auto-swap-potions'))
					removeGloomgrowthPotion();
			}
		};

		const harvestPlot = (plot) => {
			const plotType = plot.category.id;

			if (config[plotType].priorityType === priorityTypes.replant.id) {
				config[plotType].lockedPlots[plot.id] = plot.plantedRecipe.id;
			}

			if (plot.state == STATE_DEAD || bankHasRoom(getProduct(plot.plantedRecipe))) {
				FARMING_SKILL.harvestPlotOnClick(plot);
			}
		};

		// Abyssal compost stuff
		const seedsAboveAbyssalCompostThreshold = (action, plot) => {
			if (plot.category.id === "melvorItA:Special" || plot.category.id === "melvorD:Tree")
				return true;

			return bankQty(action.seedCost.item) >= (action.seedCost.quantity * SETTING_COMPOST.get('abyssal-compost-use-threshold')); 
		};

		const shouldDestroyAbyssalCompost = (action, plot) => {
			if (plot.category.id === "melvorItA:Special" || plot.category.id === "melvorD:Tree")
				return false;

			if (plot.compostItem == itemAbyssalCompost)
				return bankQty(action.seedCost.item) < (action.seedCost.quantity * SETTING_COMPOST.get('abyssal-compost-destroy-threshold'));

			return false;
		};

		const seedPlot = (plot) => {
			let seedAction = getNextSeedAction(plot);

			if (!seedAction)
				return;

			// Remove abyssal compost if needed
			if (hasIntoTheAbyss && shouldDestroyAbyssalCompost(seedAction, plot))
				FARMING_SKILL.removeCompostFromPlot(plot);

			// Apply compost
			if (plot.compostLevel < 100) {
				if (seedAction.namespace === "melvorItA"
					&& hasIntoTheAbyss // Probably not needed, but just in case 
					&& SETTING_COMPOST.get('use-abyssal-compost') 
					&& plot.compostItem != itemAbyssalCompost && bankQty(itemAbyssalCompost) >= 10
					&& seedsAboveAbyssalCompostThreshold(seedAction, plot)) {
					FARMING_SKILL.compostPlot(plot, itemAbyssalCompost, 10);
				}
				else if (SETTING_COMPOST.get('use-weird-gloop')
					&& plot.compostItem != itemWeirdGloop
					&& bankQty(itemWeirdGloop) > 1) {
					FARMING_SKILL.compostPlot(plot, itemWeirdGloop, 1);
				}
				else if (SETTING_COMPOST.get('use-compost') && canCropDie(seedAction, plot)) {
					buyCompost();

					if (bankQty(itemCompost) > 0)
						FARMING_SKILL.compostPlot(plot, itemCompost, 5);
				}
			}

			FARMING_SKILL.plantRecipe(seedAction, plot);
		};

		const getNextSeedAction = (plot) => {
			// Find next seed in bank according to priority
			const plotType = plot.category.id;
			const plotTypeConfig = config[plotType];

			const lockedSeed = plotTypeConfig.lockedPlots[plot.id];
			let actions = [];

			if (lockedSeed !== undefined) {
				actions = [getAction(lockedSeed)];
			}
			else if (plotTypeConfig.priorityType === priorityTypes.custom.id) {
				actions = PRIORITY_ARRAYS[plotType];
			}
			else if (plotTypeConfig.priorityType === priorityTypes.mastery.id) {
				actions = FARMING_ACTIONS[plotType]
					.filter(action => !config.disabledActions[action.id] && getMasteryLevel(action) < 99)
					.sort((a, b) => getMasteryXP(b) - getMasteryXP(a));
			}
			else if (plotTypeConfig.priorityType === priorityTypes.lowestQuantity.id) {
				// Special seeds produce dropTables so they need to have their drops checked for quantity instead of the product itself.
				if (plotType === "melvorItA:Special") {
					actions = FARMING_ACTIONS[plotType]
						.filter(action => !config.disabledActions[action.id])
						.map(action => {
							const product = getProduct(action);
							let minQuantity;
			
							// Check if the product has a dropTable with multiple drops
							if (product.dropTable && product.dropTable.drops) {
								// Get minimum quantity among all items in dropTable
								minQuantity = Math.min(...product.dropTable.drops.map(drop => bankQty(drop.item)));
							} else {
								// For base products, use the bankQty directly
								minQuantity = bankQty(product);
							}
			
							return { action, minQuantity };
						})
						.sort((a, b) => a.minQuantity - b.minQuantity)
						.map(entry => entry.action);
				} else {
					actions = FARMING_ACTIONS[plotType]
						.filter(action => !config.disabledActions[action.id])
						.sort((a, b) => bankQty(getProduct(a)) - bankQty(getProduct(b)));
				}
			}			

			//for (let k = 0; k < actions.length; k++) {
			//	debugLog(actions[k].name, getMasteryXP(actions[k]).toFixed(3), bankQty(getProduct(actions[k])));
			//}

			let nextSeed = null;
			for (let k = 0; k < actions.length; k++) {
				const action = actions[k];
				if (actionIsValid(FARMING_SKILL, action) && canAfford(action)) {
					nextSeed = action;
					break;
				}
			}

			return nextSeed;
		}

		// ui
		const htmlID = (id) => id.replace(/[:_]/g, '-').toLowerCase();

		const orderMasteryPriorityMenu = (category) => {
			const menu = $(`#${id}-${htmlID(category.id)}-prioritysettings-mastery`);
			menu.children()
				.toArray()
				.filter((e) => getMasteryLevel(getAction($(e).data('action'))) >= 99)
				.forEach((e) => $(e).remove());
			const sortedMenuItems = menu
				.children()
				.toArray()
				.sort((a, b) => getMasteryXP(getAction($(b).data('action'))) - getMasteryXP(getAction($(a).data('action'))));
			menu.append(sortedMenuItems);
		};

		const orderCustomPriorityMenu = (category) => {
			const priority = PRIORITY_ARRAYS[category.id];
			if (!priority.length) {
				return;
			}
			const menu = $(`#${id}-${htmlID(category.id)}-prioritysettings-custom`);
			const menuItems = [...menu.children()];

			function indexOfOrInf(el) {
				let i = priority.indexOf(el);
				return i === -1 ? Infinity : i;
			}

			const sortedMenu = menuItems.sort(
				(a, b) => indexOfOrInf(getAction($(a).data('action'))) - indexOfOrInf(getAction($(b).data('action')))
			);
			menu.append(sortedMenu);
		};

		const showReservedButtonInCategory = (category) => {
			const plots = FARMING_SKILL.getPlotsForCategory(category);
			let i = 0;
			plots.every((plot) => {
				if (plot.state !== STATE_LOCKED) {
					if (farmingMenus.plots[i].buttonCheckBox === undefined) {
						const buttonId = id + '-' + htmlID(plot.id) + '-plot-reserved'
						const buttonContainer = createElement('div', {
							className: 'custom-control custom-switch',
							children: [createElement('label', {
								className: 'custom-control-label',
								attributes: [['for', buttonId]],
								children: ['Ignore']
							})]
						});
						const buttonCheckBox = createElement('input', {
							className: 'custom-control-input',
							attributes: [['type', 'checkbox'], ['id', buttonId]],
						});
						buttonContainer.prepend(buttonCheckBox);

						tippy(buttonContainer, {
							content: `SEMI Auto-Farming will ignore this plot.`,
							placement: 'top',
							allowHTML: true,
							interactive: false,
							animation: false,
						});

						farmingMenus.plots[i].buttonCheckBox = buttonCheckBox;
						farmingMenus.plots[i].categoryName.after(buttonContainer);
						farmingMenus.plots[i].categoryName.parentNode.style.flexWrap = "wrap";
					}

					farmingMenus.plots[i].buttonCheckBox.checked = config[plot.category.id].reservedPlots[plot.id] !== undefined;
					farmingMenus.plots[i].buttonCheckBox.onclick = () => {
						if (config[plot.category.id].reservedPlots[plot.id] !== undefined) {
							delete config[plot.category.id].reservedPlots[plot.id];
						} else {
							config[plot.category.id].reservedPlots[plot.id] = 'Reserved';
						}
						storeConfig();
					};
					i++;
				}
				return true;
			});
		}

		const showAutoFarmSettings = (typeID) => {
			const catID = htmlID(typeID);
			$(`#${id} > div`).addClass('d-none');
			$(`#${id}-${catID}`).removeClass('d-none');
		}

		const injectGUI = () => {
			$(`#${id}`).remove();

			const disabledOpacity = 0.25;

			function createPatchTypeDiv(category) {
				const catID = htmlID(category.id);

				function createSeedDiv(action) {
					return `
							<div class="btn btn-outline-secondary ${id}-priority-selector" data-action="${action.id}" data-tippy-content="${action.name}" style="margin: 2px; padding: 6px; float: left;">
								<img src="${action.product.media}" width="30" height="30">
							</div>`;
				}

				function createPriorityTypeSelector(priorityType) {
					const prefix = `${id}-${catID}-prioritytype`;
					const elementId = `${prefix}-${priorityType.id}`;
					return `<div class="custom-control custom-radio custom-control-inline" data-tippy-content="${priorityType.tooltip}">
								<input class="custom-control-input" type="radio" id="${elementId}" name="${prefix}" value="${priorityType.id}"${config[category.id].priorityType === priorityType.id ? ' checked' : ''}>
								<label class="custom-control-label" for="${elementId}">${priorityType.description}</label>
							</div>`;
				}

				const prefix = `${id}-${catID}`;
				const prioritySettings = `${prefix}-prioritysettings`;
				const seedDivs = FARMING_ACTIONS[category.id].map(createSeedDiv).join('');

				return `<div id="${prefix}" class="col-12 p-0 d-none">
						<div class="block block-rounded block-link-pop border-top border-herblore border-4x" style="padding-bottom: 12px;">
							<div class="block-header border-bottom">
								<h3 class="block-title">AutoFarm ${category.name}</h3>
								<div class="custom-control custom-switch">
									<input type="checkbox" class="custom-control-input" id="${prefix}-enabled" name="${prefix}-enabled"${config[category.id].enabled ? ' checked' : ''}>
									<label class="custom-control-label" for="${prefix}-enabled">Enable</label>
								</div>
							</div>
							<div class="block-content" style="padding-top: 12px">
								${Object.values(priorityTypes).map(createPriorityTypeSelector).join('')}
							</div>
							<div class="block-content" style="padding-top: 12px">
								<div id="${prioritySettings}-custom">
									${seedDivs}
									<button id="${prioritySettings}-reset" class="btn btn-primary locked" data-tippy-content="Reset order to default (highest to lowest level)" style="margin: 5px 0 0 2px; float: right;">Reset</button>
								</div>
								<div id="${prioritySettings}-mastery" class="${id}-seed-toggles">
									${seedDivs}
								</div>
								<div id="${prioritySettings}-lowestQuantity" class="${id}-seed-toggles">
									${seedDivs}
								</div>
							</div>
						</div>
					</div>`;
			}

			const autoFarmDiv = `<div id="${id}" class="row row-deck gutters-tiny">${FARMING_CATEGORIES.map(createPatchTypeDiv).join('')}</div>`;
			$('#farming-category-container').after($(autoFarmDiv));

			showAutoFarmSettings(FARMING_SKILL.visibleCategory.id);
			showReservedButtonInCategory(FARMING_SKILL.categories.firstObject);

			function addStateChangeHandler(category) {
				$(`#${id}-${htmlID(category.id)}-enabled`).change((event) => {
					config[category.id].enabled = event.currentTarget.checked;
					addTimer(5);
					storeConfig();
				});
			}
			FARMING_CATEGORIES.forEach(addStateChangeHandler);

			function showSelectedPriorityTypeSettings(category) {
				for (const priorityType of Object.values(priorityTypes)) {
					$(`#${id}-${htmlID(category.id)}-prioritysettings-${priorityType.id}`).toggle(
						priorityType.id === config[category.id].priorityType
					);
				}
			}
			FARMING_CATEGORIES.forEach(showSelectedPriorityTypeSettings);

			function lockPatch(category, plot, action) {
				if (action !== undefined) {
					config[category.id].lockedPlots[plot.id] = action.id;
				}
				else {
					delete config[category.id].lockedPlots[plot.id];
				}
			}

			function addPriorityTypeChangeHandler(category) {
				function lockAllPlots(auto = false) {
					FARMING_PLOTS[category.id].forEach(plot => {
						lockPatch(category, plot, auto ? undefined : plot.plantedRecipe);
					});
					$(`.${id}-seed-selector`).remove();
				}

				$(`#${id} input[name="${id}-${htmlID(category.id)}-prioritytype"]`).change((event) => {
					if (config[category.id].priorityType === priorityTypes.replant.id) {
						lockAllPlots(true);
					}

					config[category.id].priorityType = event.currentTarget.value;
					if (event.currentTarget.value === priorityTypes.replant.id) {
						lockAllPlots();
					}
					showSelectedPriorityTypeSettings(category);
					storeConfig();
				});
			}
			FARMING_CATEGORIES.forEach(addPriorityTypeChangeHandler);

			function makeSortable(category) {
				const elementId = `${id}-${htmlID(category.id)}-prioritysettings-custom`;
				Sortable.create(document.getElementById(elementId), {
					animation: 150,
					filter: '.locked',
					onMove: (event) => {
						if (event.related) {
							return !event.related.classList.contains('locked');
						}
					},
					onEnd: () => {
						PRIORITY_ARRAYS[category.id] = [...$(`#${elementId} .${id}-priority-selector`)].map(
							(x) => getAction($(x).data('action'))
						);
						config[category.id].priority = priorityToConfig(PRIORITY_ARRAYS[category.id]);
						storeConfig();
					},
				});
			}
			FARMING_CATEGORIES.forEach(makeSortable);

			function addPriorityResetClickHandler(category) {
				$(`#${id}-${htmlID(category.id)}-prioritysettings-reset`).on('click', () => {
					PRIORITY_ARRAYS[category.id] = [...FARMING_ACTIONS[category.id]];
					config[category.id].priority = priorityToConfig(PRIORITY_ARRAYS[category.id]);
					orderCustomPriorityMenu(category);
					storeConfig();
				});
			}
			FARMING_CATEGORIES.forEach(addPriorityResetClickHandler);

			$(`.${id}-seed-toggles div`).each((_, e) => {
				const toggle = $(e);
				const actionID = toggle.data('action');
				if (config.disabledActions[actionID]) {
					toggle.css('opacity', disabledOpacity);
				}
			});

			$(`.${id}-seed-toggles div`).on('click', (event) => {
				const toggle = $(event.currentTarget);
				const actionID = toggle.data('action');
				if (config.disabledActions[actionID]) {
					delete config.disabledActions[actionID];
				}
				else {
					config.disabledActions[actionID] = true;
				}
				const opacity = config.disabledActions[actionID] ? disabledOpacity : 1;
				toggle.fadeTo(200, opacity);
				storeConfig();
			});

			FARMING_CATEGORIES.forEach(category => {
				orderCustomPriorityMenu(category);
				orderMasteryPriorityMenu(category);
			});

			tippy(`#${id} [data-tippy-content]`, {
				animation: false,
				allowHTML: true
			});
		};

		// config
		const storeConfig = () => {
			ctx.characterStorage.setItem('config', config);
		}

		const loadConfig = () => {
			const storedConfig = ctx.characterStorage.getItem('config');
			if (!storedConfig) {
				return;
			}

			config = { ...config, ...storedConfig };

			FARMING_CATEGORIES.forEach(category => {
				if (config[category.id] && config[category.id].priority) {
					PRIORITY_ARRAYS[category.id] = priorityFromConfig(config[category.id].priority);
				}

				if (config[category.id].reservedPlots == undefined) {
					config[category.id].reservedPlots = {};
				}
			});
		}

		// settings
		SETTING_COMPOST.add([
			{
				'type': 'switch',
				'name': `compost-buy`,
				'label': `Buy Compost if none available.`,
				'default': true
			},
			{
				'type': 'switch',
				'name': `use-compost`,
				'label': `Use Compost`,
				'hint': `Compost will not be used when Mastery Pool has reach 25%, or a seeds Mastery has reached level 50.`,
				'default': true
			},
			{
				'type': 'switch',
				'name': `use-weird-gloop`,
				'label': `Use Weird Gloop`,
				'default': true
			},
		]);

		if (hasIntoTheAbyss) {
			SETTING_COMPOST.add([
				{
					'type': 'switch',
					'name': `use-abyssal-compost`,
					'label': `Use Abyssal Compost`,
					'description': `Use Abyssal Compost only when you can plant seeds 1000 times, it will be automatically destroyed when seeds can only be planted 100 times.`,
					'default': true
				},
				{
					'type': 'number',
					'name': 'abyssal-compost-use-threshold',
					'label': 'Abyssal Compost Use Threshold',
					'hint': 'Threshold for using Abyssal Compost. For example, 1000 will use Abyssal Compost when you can plant at least 1000 times.',
					'default': 1000,
					'min': 0,
					'onChange': (newValue, previousValue) => {
					  const destroyThreshold = SETTING_COMPOST.get('abyssal-compost-destroy-threshold');
					  if (newValue <= destroyThreshold) {
						return false;
					  }
					  return true;
					}
				},
				{
					'type': 'number',
					'name': 'abyssal-compost-destroy-threshold',
					'label': 'Abyssal Compost Destroy Threshold',
					'hint': 'Threshold for destroying Abyssal Compost. For example, 100 will destroy when you can plant at most 100 times.',
					'default': 100,
					'min': 0,
					'onChange': (newValue, previousValue) => {
					  const useThreshold = SETTING_COMPOST.get('abyssal-compost-use-threshold');
					  if (newValue >= useThreshold) {
						return false;
					  }
					  return true;
					}
				}
			]);
		}

		SETTING_POTION.add([{
			'type': 'switch',
			'name': `auto-swap-potions`,
			'label': `Automatically swap farming potions to ensure the best yield when harvesting.`,
			'default': true
		}]);

		SETTING_EQUIPMENT.add({
			'name': 'auto-swap-equipment-description',
			'type': 'label',
			'label': 'Swap to and from equipment when not in combat before handling any plots.'
		});
		
		const allGearSlotKeys = new Set([
			...Object.keys(gearPriorityIDs),
			...(hasIntoTheAbyss ? Object.keys(abyssalGearPriorityIDs) : [])
		]);		  

		allGearSlotKeys.forEach(slotKey => {
			const slot = game.equipmentSlots.getObjectByID(slotKey);

			if (slot) {
				const regularItems = melvorGearPriority[slotKey] || [];
				const abyssalItems = abyssalGearPriority[slotKey] || [];
				const combinedItems = Array.from(new Set([...regularItems, ...(hasIntoTheAbyss ? abyssalItems : [])]));
				
				SETTING_EQUIPMENT.add({
					'type': 'switch',
					'name': `swap-${slotKey}`,
					'label': `Swap ${slot.emptyName}`,
					'hint': `[${combinedItems.map(item => item.name).join(', ')}]`,
					'default': true
				});
			}
		});

		// game hooks
		let CURRENT_TICK = 50;
		const farmingPassiveTick = () => {
			CURRENT_TICK--;
			if (CURRENT_TICK <= 0) {
				handlePlots();
				removeTimer();
			}
		};

		const passiveTimer = {
			"id": "SEMI:AutoFarming",
			"localID": "AutoFarming",
			"namespace": "SEMI",
			"isModded": true,
			"activeTick": () => { },
			"passiveTick": () => { farmingPassiveTick(); }
		};

		const addTimer = (ticks) => {
			CURRENT_TICK = ticks;
			if (game._passiveTickers.indexOf(passiveTimer) == -1) {
				game._passiveTickers.push(passiveTimer);
			}
		}

		const removeTimer = () => {
			let idx = game._passiveTickers.indexOf(passiveTimer);
			if (idx != -1 && CURRENT_TICK == 0) {
				game._passiveTickers.splice(idx, 1);
			}
		}

		ctx.onCharacterLoaded(() => {
			loadConfig();

			addTimer(20);

			// add reserved button
			ctx.patch(Farming, 'showPlotsInCategory').after(function (_, category) {
				showReservedButtonInCategory(category);
			});

			// show category settings
			ctx.patch(Farming, 'showPlotsInCategory').after(function (_, category) {
				showAutoFarmSettings(category.id);
			});

			// crop finished
			ctx.patch(Farming, 'growPlots').after(function (_, timer) {
				addTimer(20);
			});

			// purchase / unlock plot
			ctx.patch(Farming, 'unlockPlotOnClick').after(function (_, plot) {
				if (plot.state === STATE_EMPTY) {
					addTimer(5);
				}
			});

			// plot reset
			ctx.patch(Farming, 'resetPlot').after(function (_, plot) {
				addTimer(20);
			});
		});

		ctx.onInterfaceReady(() => {
			injectGUI();
		});
	});
}
